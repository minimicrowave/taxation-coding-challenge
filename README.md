# OnlinePajak Coding Challenge

1. Clone repository
```
https://gitlab.com/minimicrowave/taxation-coding-challenge.git
```

2. Change into repository and install dependencies
```
cd taxation-coding-challenge
npm i
```

3. Run tests
```
npm run test
```