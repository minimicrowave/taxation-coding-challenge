const { expect } = require('chai');
const TaxCalculator = require('../src/TaxCalculator');
const { INVALID_DEPENDANT_STATUS, INVALID_TYPE_NUMBER } = require('../src/consts/ErrorMessages');
const errorMessageFormatter = require('../src/formatter/ErrorMessageFormatter');

describe('TaxCalculator', () => {
	describe('Error Handling', () => {
		it('should return "{argument} is not a number."', () => {
			expect(() => new TaxCalculator().compute()).to.throw(
				errorMessageFormatter('Monthly Income', INVALID_TYPE_NUMBER)
			);
			expect(() => new TaxCalculator(null, 'TK0').compute()).to.throw(
				errorMessageFormatter('Monthly Income', INVALID_TYPE_NUMBER)
			);
			expect(() => new TaxCalculator(null, 'K3').compute()).to.throw(
				errorMessageFormatter('Monthly Income', INVALID_TYPE_NUMBER)
			);
		});
		it('should return "Dependant status is not valid."', () => {
			expect(() => new TaxCalculator(1220000, '').compute()).to.throw(INVALID_DEPENDANT_STATUS);
			expect(() => new TaxCalculator(2200000).compute()).to.throw(INVALID_DEPENDANT_STATUS);
			expect(() => new TaxCalculator(3300000, 'K').compute()).to.throw(INVALID_DEPENDANT_STATUS);
		});
	});

	describe('.compute()', () => {
		it('should return 31900000 if monthly income is 25000000, and single (TK0)', () => {
			expect(new TaxCalculator(25000000, 'TK0').compute()).to.equal(31900000);
		});
		it('should return 144800000 if monthly income is 60000000, and single (TK0)', () => {
			expect(new TaxCalculator(60000000, 'TK0').compute()).to.equal(144800000);
		});
		it('should return 45375000 if monthly income is 30000000, is married with no children (K0)', () => {
			expect(new TaxCalculator(30000000, 'K0').compute()).to.equal(45375000);
		});
		it('should return 750000 if monthly income is 6500000, and has 1 child (K1)', () => {
			expect(new TaxCalculator(6500000, 'K1').compute()).to.equal(750000);
		});
		it('should return 0 if monthly income is 40000000, and has 2 dependants (K2)', () => {
			expect(new TaxCalculator(4000000, 'K2').compute()).to.equal(0);
		});
		it('should return 42000000 if monthly income is 30000000, and has 3 dependants (K3)', () => {
			expect(new TaxCalculator(30000000, 'K3').compute()).to.equal(42000000);
		});
	});
});
