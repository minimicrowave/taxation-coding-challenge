const validate = require('./validation');
const { isNull } = require('./utils');
const {
	INCOME_TAX_RATE_THRESHOLD,
	DEPENDANT_TAX_RELIEF_MAP,
	MINIMUM_TAXABLE_INCOME
} = require('./consts/Tax');

class TaxCalculator {
	constructor(monthlyIncome, dependantStatus) {
		validate(monthlyIncome, dependantStatus);
		this.yearlyIncome = monthlyIncome * 12;
		this.dependantStatus = dependantStatus;
	}

	compute() {
		let taxableAnnualIncome = this.getTaxableAnnualIncome();
		return this.getTotalAnnualTaxPayable(taxableAnnualIncome);
	}

	getTaxableAnnualIncome() {
		let taxableAnnualIncome = this.yearlyIncome - DEPENDANT_TAX_RELIEF_MAP[this.dependantStatus];
		return taxableAnnualIncome > MINIMUM_TAXABLE_INCOME
			? taxableAnnualIncome
			: MINIMUM_TAXABLE_INCOME;
	}

	getTotalAnnualTaxPayable(taxableAnnualIncome) {
		let totalTaxPayable = 0;

		INCOME_TAX_RATE_THRESHOLD.some(({ top, bottom, rate }) => {
			if (!isNull(top) && taxableAnnualIncome > top) {
				totalTaxPayable += (top - bottom) * rate;
				return false;
			} else {
				totalTaxPayable += (taxableAnnualIncome - bottom) * rate;
				return true;
			}
		});

		return totalTaxPayable;
	}
}

module.exports = TaxCalculator;
