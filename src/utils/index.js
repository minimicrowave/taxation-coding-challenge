function isNumber(value) {
	return typeof value === 'number';
}

function isNull(value) {
	return value === null;
}

module.exports = { isNumber, isNull };
