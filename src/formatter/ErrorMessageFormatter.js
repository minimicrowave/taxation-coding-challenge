function errorMessageFormatter(value, errorMessage) {
	return errorMessage.replace('{value}', value);
}

module.exports = errorMessageFormatter;
