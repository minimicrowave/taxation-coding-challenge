const { isNumber } = require('../utils');
const errorMessageFormatter = require('../formatter/ErrorMessageFormatter');
const { INVALID_TYPE_NUMBER, INVALID_DEPENDANT_STATUS } = require('../consts/ErrorMessages');
const { DEPENDANT_TAX_RELIEF_MAP } = require('../consts/Tax');

const DEPENDANT_STATUS_LIST = () => Object.keys(DEPENDANT_TAX_RELIEF_MAP);

function validate(monthlyIncome, dependantStatus) {
	if (!isNumber(monthlyIncome))
		throw new Error(errorMessageFormatter(`Monthly Income`, INVALID_TYPE_NUMBER));
	if (!DEPENDANT_STATUS_LIST().includes(dependantStatus)) throw new Error(INVALID_DEPENDANT_STATUS);
}

module.exports = validate;
