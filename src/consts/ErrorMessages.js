const ErrorMessages = {
	INVALID_TYPE_NUMBER: '{value} is not a number.',
	INVALID_DEPENDANT_STATUS: 'Dependant status is not valid.'
};

module.exports = ErrorMessages;
