// Maps the maximum and minimum threshold of income and their respective tax rate
const INCOME_TAX_RATE_THRESHOLD = [
	{
		top: 50000000,
		bottom: 0,
		rate: 0.05
	},
	{
		top: 250000000,
		bottom: 50000000,
		rate: 0.15
	},
	{
		top: 500000000,
		bottom: 250000000,
		rate: 0.25
	},
	{
		top: null,
		bottom: 500000000,
		rate: 0.3
	}
];

const DEPENDANT_TAX_RELIEF_MAP = {
	TK0: 54000000,
	K0: 58500000,
	K1: 63000000,
	K2: 67500000,
	K3: 72000000
};

const MINIMUM_TAXABLE_INCOME = 0;

module.exports = { INCOME_TAX_RATE_THRESHOLD, DEPENDANT_TAX_RELIEF_MAP, MINIMUM_TAXABLE_INCOME };
